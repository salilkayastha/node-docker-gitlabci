FROM node:18

# create dir
# RUN mkdir -p /home/node/app/node_modules && chown -R node:node /home/node/app
WORKDIR /app

# build dependencies
COPY ./package*.json ./
# USER node
RUN npm install
# RUN npm audit fix --force

# create static configuration for app
RUN echo "variableData=Dockerfile-Build" >> .env

# copy in source code
COPY ./ ./

# start express server
CMD [ "npm", "start" ]
